# Qu Challenge

Objective: The objective of this challenge is not necessarily just to solve the problem but to evaluate your software development skills, code quality, analysis, creativity, and resourcefulness as a potential future colleague. Please share the necessary artifacts you would provide to your colleagues in a real-world professional setting to best evaluate your work.

## Test and Deploy

Open and Run QuChallenge project. It's a console app.

