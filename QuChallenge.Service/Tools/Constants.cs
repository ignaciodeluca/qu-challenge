﻿namespace QuChallenge.Service.Tools
{
    public static class Constants
    {
        public static int MaxSize = 64;
        public static string MatrixNullMessage = "Matrix is null.";
        public static string MatrixSizeExceptionMessage = "Matrix too big!";
        public static string MatrixWidthExceptionMessage = "All strings must contain the same number of characters!";
        public static int MaxResult = 10;
    }
}
