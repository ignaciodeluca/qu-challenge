﻿using QuChallenge.Service.Exceptions;
using QuChallenge.Service.ExtensionMethods;
using QuChallenge.Service.Tools;
using System.Text.RegularExpressions;

namespace QuChallenge.Service
{
    public class WordFinder : IWordFinder
    {
        #region Fields

        IEnumerable<string> _matrix;

        #endregion

        /// <summary>
        /// Constructor.
        /// Receives a set of strings which represents a character matrix.        
        /// </summary>
        /// <param name="matrix"></param>
        public WordFinder(IEnumerable<string> matrix) 
        {
            _matrix = matrix;
        }

        /// <summary>
        /// Return the top 10 most repeated words from the word stream found in the matrix.
        /// If no words are found, should return an empty set of strings. If any word in the word stream 
        /// is found more than once within the stream, the search results should count it only once
        /// </summary>
        /// <param name="wordstream"></param>
        /// <returns></returns>
        public IEnumerable<string> Find(IEnumerable<string> wordstream)
        {
            ValidateMatrix();

            if (IsEmptyMatrix())
                return new List<string>();

            int matrixHeight = _matrix.Count();
            Dictionary<string, int> wordsFound = new Dictionary<string, int>();

            for (int matrixIndex = 0; matrixIndex < matrixHeight; matrixIndex++)
            {
                string line = _matrix.ElementAt(matrixIndex);

                foreach (string word in wordstream)
                {
                    char firstLetter = word.ElementAt(0);
                    IEnumerable<int> letterIndexes = line.AllIndexesOf(firstLetter.ToString());

                    //add horizontally
                    AddWord(ref wordsFound, word, CountWordInLine(word, line));

                    //add vertically
                    foreach (int index in letterIndexes)
                    {
                        if (IsWordVertically(word, 0, matrixIndex, matrixHeight, index))
                        {
                            AddWord(ref wordsFound, word);
                            continue;
                        }
                    }
                }
            }

            return ReturnResult(wordsFound);
        }

        #region Private methods

        /// <summary>
        /// Check if matrix is empty
        /// </summary>
        /// <returns></returns>
        private bool IsEmptyMatrix()
        {
            return !_matrix.Any();
        }

        /// <summary>
        /// Check if matrix is valid
        /// The matrix size does not exceed 64x64, all strings contain the same number of characters.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="MatrixSizeException"></exception>
        /// <exception cref="MatrixWidthException"></exception>
        private void ValidateMatrix()
        {
            if (_matrix == null)
                throw new ArgumentNullException(Constants.MatrixNullMessage);

            if (!IsEmptyMatrix())
            {
                int matrixWidth = _matrix.First().Length;

                if (_matrix.Count() >= Constants.MaxSize
                    || matrixWidth >= Constants.MaxSize)
                    throw new MatrixSizeException(Constants.MatrixSizeExceptionMessage);

                if (_matrix.Any(x => x.Length != matrixWidth))
                    throw new MatrixWidthException(Constants.MatrixWidthExceptionMessage);
            }                
        }

        /// <summary>
        /// Gets the number of matches of the word in the line
        /// </summary>
        /// <param name="word"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        private int CountWordInLine(string word, string line)
        {
            return Regex.Matches(line, word).Count;
        }

        /// <summary>
        /// Check if the word is in vertical way
        /// </summary>
        /// <param name="word"></param>
        /// <param name="wordIndex"></param>
        /// <param name="matrixIndex"></param>
        /// <param name="matrixHeight"></param>
        /// <param name="indexInLine"></param>
        /// <returns></returns>
        private bool IsWordVertically(string word, int wordIndex, int matrixIndex, int matrixHeight, int indexInLine)
        {
            if (matrixIndex >= matrixHeight)
                return false;

            if (word.Length - 1 == wordIndex)
                return true;

            string line = _matrix.ElementAt(matrixIndex);

            if (word[wordIndex] == line[indexInLine])
                return IsWordVertically(word, wordIndex + 1, matrixIndex + 1, matrixHeight, indexInLine);

            return false;
        }

        /// <summary>
        /// Add the word found to the list of results
        /// </summary>
        /// <param name="wordsFound"></param>
        /// <param name="word"></param>
        /// <param name="quantity"></param>
        private void AddWord(ref Dictionary<string, int> wordsFound, string word, int quantity = 1)
        {
            if (quantity == 0)
                return;

            if (wordsFound.TryGetValue(word, out int value))
                wordsFound[word] = value + quantity;
            else
                wordsFound.Add(word, quantity);
        }

        /// <summary>
        /// Return the top 10 most repeated words from the word stream found in the matrix.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> ReturnResult(Dictionary<string, int> wordsFound)
        {
            IEnumerable<string> result = new List<string>();
            int quantityResult = 1;

            foreach (var item in wordsFound.OrderByDescending(x => x.Value))
            {
                if (quantityResult > Constants.MaxResult)
                    break;

                result = result.Add(item.Key);

                quantityResult++;
            }

            return result;
        }

        #endregion
    }
}
