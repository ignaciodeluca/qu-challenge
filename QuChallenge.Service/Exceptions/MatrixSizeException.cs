﻿namespace QuChallenge.Service.Exceptions
{
    public class MatrixSizeException : Exception
    {
        
        public MatrixSizeException()
        {
        }

        public MatrixSizeException(string message)
            : base(message)
        {
        }

        public MatrixSizeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
