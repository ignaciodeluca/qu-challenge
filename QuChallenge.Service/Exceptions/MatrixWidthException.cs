﻿namespace QuChallenge.Service.Exceptions
{
    public class MatrixWidthException : Exception
    {

        public MatrixWidthException()
        {
        }

        public MatrixWidthException(string message)
            : base(message)
        {
        }

        public MatrixWidthException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
