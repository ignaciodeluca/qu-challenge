﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using QuChallenge.Service;

List<string> matrix = new List<string>
            {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy",
            };

//setup our DI
var serviceProvider = new ServiceCollection()
            .AddLogging()
            .AddSingleton<IWordFinder>(service => new WordFinder(matrix))
            .BuildServiceProvider();

//configure console logging
serviceProvider
    .GetService<ILoggerFactory>();

var logger = serviceProvider.GetService<ILoggerFactory>()
    .CreateLogger<Program>();

logger.LogDebug("Starting application");

//do the actual work here
List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

IWordFinder wordFinderService = serviceProvider.GetService<IWordFinder>();

var result = wordFinderService.Find(wordstream);

foreach (var item in result)
{
    Console.WriteLine(item);
}

Console.ReadLine();

logger.LogDebug("All done!");
