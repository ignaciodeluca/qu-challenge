using QuChallenge.Service;
using QuChallenge.Service.Exceptions;

namespace QuChallenge.UnitTest.Services
{
    public class WordFinderUnitTest
    {
        [Fact]
        public void Find_EmptyMatrix_ReturnsEmptyList()
        {
            //arrange
            List<string> matrix = new List<string>();

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            IEnumerable<string> result = wordFinder.Find(wordstream);

            //assert
            Assert.Empty(result);
        }

        [Fact]
        public void Find_ExampleMatrix_ReturnsList3words()
        {
            //arrange
            List<string> matrix = new List<string>
            {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy",
            };

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            IEnumerable<string> result = wordFinder.Find(wordstream);

            //assert
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void Find_MatrixNull_ThrowsException()
        {
            //arrange
            List<string> matrix = null;

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            Action action = () => wordFinder.Find(wordstream);

            //assert
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(action);

            Assert.Equal(Service.Tools.Constants.MatrixNullMessage, exception.ParamName);
        }

        [Fact]
        public void Find_MatrixTooBig_ThrowsException()
        {
            //arrange
            List<string> matrix = new List<string>
            {
                "abcdcefhijabcdcefhijabcdcefhijabcdcefhijabcdcefhijabcdcefhijabcdcefhij", //70
            };

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            Action action = () => wordFinder.Find(wordstream);

            //assert
            MatrixSizeException exception = Assert.Throws<MatrixSizeException>(action);

            Assert.Equal(Service.Tools.Constants.MatrixSizeExceptionMessage, exception.Message);
        }

        [Fact]        
        public void Find_MatrixWithDifferentWidth_ThrowsException()
        {
            //arrange
            List<string> matrix = new List<string>
            {
                "abcdce", //6
                "fgwio", //5
                "chill", //5
                "pqnsd", //5
                "uvdxy", //5
            };

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            Action action = () => wordFinder.Find(wordstream);

            //assert
            MatrixWidthException exception = Assert.Throws<MatrixWidthException>(action);

            Assert.Equal(Service.Tools.Constants.MatrixWidthExceptionMessage, exception.Message);
        }

        [Fact]
        public void Find_ExampleMatrixWithWordMoreThanOnce_ReturnsList3words()
        {
            //arrange
            List<string> matrix = new List<string>
            {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy",
                "chill",
                "colde",
            };

            List<string> wordstream = new List<string>
            {
                "cold",
                "wind",
                "snow",
                "chill"
            };

            //act
            WordFinder wordFinder = new WordFinder(matrix);

            IEnumerable<string> result = wordFinder.Find(wordstream);

            //assert
            Assert.Equal(3, result.Count());
        }
    }
}